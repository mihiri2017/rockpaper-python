from tkinter import *
import random
root = Tk()
root.title("Rock Scissors Paper")
root.geometry("900x700+200+0")


global score1count, score2count
score1count = 0
score2count = 0

choice1 = IntVar()
choice2 = IntVar()
player = IntVar()
player.set(2)

score1 = StringVar()
score1.set(str(score1count))
score2 = StringVar()
score2.set(str(score2count))

name1 = StringVar()
name1.set("Player 1")
name2 = StringVar()
name2.set("Computer")

choices = ["Rock", "Scissors", "Paper"]



def changeColor1():
    Game1.config(bg="green")
    player1.config(bg="green")
    lblscore1.config(bg="green")
    # print(choice1.get())

    if player.get() == 2:
        com = random.randint(1, 3)
        if (choice1.get() == 1) & (com == 2):
            comst.config(text="Scissor")
            onebeatstwo()
        if (choice1.get() == 2) & (com == 3):
            comst.config(text="Paper")
            onebeatstwo()
        if (choice1.get() == 3) & (com == 1):
            comst.config(text="Rock")
            onebeatstwo()

        if (choice1.get() == 2) & (com == 1):
            comst.config(text="Rock")
            twobeatsone()
        if (choice1.get() == 3) & (com == 2):
            comst.config(text="Scissor")
            twobeatsone()
        if (choice1.get() == 1) & (com == 3):
            comst.config(text="Paper")
            twobeatsone()

        if choice1.get() == com:
            lblinfo.config(text="Tie Game", fg="red")


def onebeatstwo():
    global score1count
    lblinfo.config(text=("%s beats %s" % (name1.get(), name2.get())), fg="red")
    score1count += 1
    score1.set(str(score1count))


def twobeatsone():
    global score2count
    lblinfo.config(text=("%s beats %s" % (name2.get(), name1.get())), fg="red")
    score2count += 1
    score2.set(str(score2count))


def changeColor2():
    if player.get() == 1:
        Game2.config(bg="green")
        player2.config(bg="green")
        lblscore2.config(bg="green")
        comst.config(fg="green", bg="green")
        if (choice1.get() == 1) & (choice2.get() == 2):
            onebeatstwo()
        if (choice1.get() == 2) & (choice2.get() == 3):
            onebeatstwo()
        if (choice1.get() == 3) & (choice2.get() == 1):
            onebeatstwo()

        if (choice1.get() == 2) & (choice2.get() == 1):
            twobeatsone()
        if (choice1.get() == 3) & (choice2.get() == 2):
            twobeatsone()
        if (choice1.get() == 1) & (choice2.get() == 3):
            twobeatsone()

        if choice1.get() == choice2.get():
            lblinfo.config(text="Tie Game", fg="red")
    else:
        print("hi")


def new():
    global score1count, score2count
    score1count = 0
    score2count = 0
    score1.set("0")
    score2.set("0")

    choice1.set(0)
    choice2.set(0)
    lblinfo.config(text="---")
    comst.config(text="Computer Statues")

    Game1.config(bg="gray94")
    player1.config(bg="gray94")
    Game2.config(bg="gray94")
    player2.config(bg="gray94")
    lblscore1.config(bg="gray94")
    lblscore2.config(bg="gray94")
    comst.config(bg="gray94", fg="gray94")


def reset():
    new()
    computer()
    player.set(0)
    txtname2.config(state=DISABLED)
    player.set(2)


def start():
    player1.config(text="%s\n" % (name1.get().capitalize()))
    player2.config(text="%s\n" % (name2.get().capitalize()))


def manual():
    txtname2.config(state=NORMAL)
    name2.set("Player 2")
    player1.config(text="%s\n" % (name1.get().capitalize()))
    player2.config(text="%s\n" % (name2.get().capitalize()))
    comst.config(fg="gray94")


def computer():
    txtname2.config(state=DISABLED)
    name2.set("Computer")
    player2.config(text="%s\n" % (name2.get().capitalize()))
    comst.config(fg="black")


# _____________________________________Frames _____________________________
Tops = Frame(root, height=100, width=1000, bd=12, relief="raise")
Tops.pack(side=TOP, fill=BOTH)
Game1 = Frame(root, height=450, width=650, bd=12, relief="raise")
Game1.pack(fill=BOTH, side=LEFT)
Game2 = Frame(root, height=450, width=650, bd=12, relief="raise")
Game2.pack(fill=BOTH, side=LEFT)
Btnlist = Frame(root, height=450, width=650)
Btnlist.pack(fill=BOTH, side=RIGHT)

# _____________________________________Top Ins _____________________________
name = Label(Tops, font=('arial', 20, 'bold'), fg='Green', text="Rock Scissors Paper")
name.pack(fill=BOTH)

ins_1 = Label(Tops, font=('arial', 10), text="\nThere are 2 players: player1 and player2. You can enter r - Rock, s - scissors and p - paper")
ins_1.pack()
ins_2 = Label(Tops, font=('arial', 10), text="The game is not case sensitive\n")
ins_2.pack()
ins_3 = Label(Tops, font=('arial', 10), text="1.Rock beats scissors.")
ins_3.pack()
ins_4 = Label(Tops, font=('arial', 10), text="2.Scissors beats paper.")
ins_4.pack()
ins_5 = Label(Tops, font=('arial', 10), text="3.Paper beats rock.\n")
ins_5.pack()
ins_6 = Label(Tops, text="Good Luck!\n", font=('arial', 15, 'bold'))
ins_6.pack()
lblinfo = Label(Tops, font=('arial', 15, 'bold'), text="---")
lblinfo.pack()

# _____________________________________ButtonList_____________________________
lblname1 = Label(Btnlist, font=('arial', 10, 'bold'), padx=16, pady=8, text="Player 1 Name")
lblname1.grid(row=0)
txtname1 = Entry(Btnlist, font=('arial', 10, 'bold'), textvariable=name1, bd=10, insertwidth=10)
txtname1.grid(row=1)
lblname2 = Label(Btnlist, font=('arial', 10, 'bold'), padx=16, pady=16, text="Player 2 Name")
lblname2.grid(row=2)
txtname2 = Entry(Btnlist, font=('arial', 10, 'bold'), textvariable=name2, bd=10, insertwidth=10, state=DISABLED)
txtname2.grid(row=3, column=0)
chkply = Radiobutton(Btnlist, font=('arial', 8, 'bold'), text="Manual", variable=player, value=1, command=manual)
chkply.grid(row=3, column=1)
chkply = Radiobutton(Btnlist, font=('arial', 8, 'bold'), text="Automatic", variable=player, value=2, command=computer)
chkply.grid(row=4, column=1)
btnstart = Button(Btnlist, font=('arial', 10, 'bold'), padx=8, pady=4, text="Start", bg="green", fg="white", bd=10, width=20, comman=start)
btnstart.grid(row=5)
btnreset = Button(Btnlist, font=('arial', 10, 'bold'), padx=8, pady=4, text="Reset", bg="green", fg="white", bd=10, width=20, command=reset)
btnreset.grid(row=6)
btnnew = Button(Btnlist, font=('arial', 10, 'bold'), padx=8, pady=4, text="New", bg="green", fg="white", bd=10, width=20, command=new)
btnnew.grid(row=7)
btnexit = Button(Btnlist, font=('arial', 10, 'bold'), padx=8, pady=4, text="exit", bg="green", fg="white", bd=10, width=20, command=exit)
btnexit.grid(row=8)

# ___________________________________Player 1 Game 1 _____________________________
player1 = Label(Game1, font=('arial', 10, 'bold'), padx=16, pady=8, text="Player 1\n")
player1.grid(row=1, column=0)
lblscore1 = Label(Game1, font=('arial', 20, 'bold'), padx=16, pady=8, bd=10, textvariable=score1, fg="red")
lblscore1.grid(row=1, column=1)

rock1 = Radiobutton(Game1, indicatoron=0, bg="yellowgreen", width=20, text="Rock", padx=20, pady=20, variable=choice1, value=1, command=changeColor1).grid()
scss1 = Radiobutton(Game1, indicatoron=0, bg="yellowgreen", width=20, text="Scissors", padx=20, pady=20, variable=choice1, value=2, command=changeColor1).grid()
paper1 = Radiobutton(Game1, indicatoron=0, bg="yellowgreen", width=20, text="paper", padx=20, pady=20, variable=choice1, value=3, command=changeColor1).grid()

# ________________________________Player 2 Game 2_____________________________
player2 = Label(Game2, font=('arial', 10, 'bold'), padx=16, pady=8, text="Computer\n")
player2.grid(row=1)
lblscore2 = Label(Game2, font=('arial', 20, 'bold'), padx=16, pady=8, bd=10, textvariable=score2, fg="red")
lblscore2.grid(row=1, column=1)

rock2 = Radiobutton(Game2, indicatoron=0, bg="yellowgreen", width=20, text="Rock", padx=20, pady=20, variable=choice2, value=1, command=changeColor2).grid()
scss2 = Radiobutton(Game2, indicatoron=0, bg="yellowgreen", width=20, text="Scissors", padx=20, pady=20, variable=choice2, value=2, command=changeColor2).grid()
paper2 = Radiobutton(Game2, indicatoron=0, bg="yellowgreen", width=20, text="paper", padx=20, pady=20, variable=choice2, value=3, command=changeColor2).grid()

comst = Label(Game2, font=('arial', 10, 'bold'), padx=16, pady=35, text="Computer Statues\n")
comst.grid(row=7)

root.mainloop()
