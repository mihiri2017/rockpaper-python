"""
break the while calling function from outside
playagain() will only continue for 3 times
"""
print("----------Welcome to Rock Paper Scissors!------------")
print("\nThere are 2 players: player1 and player2. You can enter r - Rock, s - scissors and p - paper")
print("\nBoth awsers should be seperated by coma: \",\" ex: r,s")
print("The game is not case sensitive")
print("\t1.Rock beats scissors. \n\t2.Scissors beats paper. \n\t2.Paper beats rock\n")


def onewon():
    print("1 beats 2")


def twowon():
    print("2 beats 1")


def playagain():
    stop = False
    while not stop:
        yesNo = str(input("Do you want to Play Again (y/n)? : ")).lower()
        if yesNo == "y":
            main()
        elif yesNo == "n":
            break
        else:
            print("Answer can be \"y\" or \"n\"")


def main():
    n = 10
    while n == 10:

        while True:  # keep looping until we break out of the loop
            try:
                player1, player2 = str(input("Enter : ")).lower().split(",")
                break  # exit the loop if the previous line succeeded
            except ValueError:
                print("Please follow the instructions")

        if player1 == "r" and player2 == "s":
            onewon()
            n = 11
        if player2 == "r" and player1 == "s":
            twowon()
            n = 11
        if player1 == "s" and player2 == "p":
            onewon()
            n = 11
        if player2 == "s" and player1 == "p":
            twowon()
            n = 11
        if player1 == "p" and player2 == "r":
            onewon()
            n = 11
        if player2 == "p" and player1 == "r":
            twowon()
            n = 11
        if n == 11:
            playagain()


main()

"""
possible_computer_choices = ['rock', 'paper', 'scissors']
computer_choice = random.choice(possible_computer_choices)
"""